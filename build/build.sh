#!/bin/bash
# -------------------------------------------------
#
# Package: Malscan
# Author: Josh Grancell <josh@malscan.com>
# Description: Linux malware scanner for web servers
# Copyright: 2015-2018 Josh Grancell
# License: MIT License
#
# -------------------------------------------------

VERSION="$1"

if [[ "$VERSION" == "ci" ]]; then
  PACKAGE_VERSION=$(cat /home/makerpm/rpmbuild/malscan-db/build/version.txt | cut -d- -f1)
  RPM_VERSION=$(cat /home/makerpm/rpmbuild/malscan-db/build/version.txt)
else
  PACKAGE_VERSION=$VERSION
fi

# Deleting everything
rm -rf "/home/makerpm/rpmbuild/BUILD/***"
rm -rf "/home/makerpm/rpmbuild/BUILDROOT/***"
rm -rf "/home/makerpm/rpmbuild/SOURCES/***"

# Creating a temp working directory
TEMP=$(mktemp -d)
mkdir -p "$TEMP/malscan-db-$VERSION"

# Moving into the malscan-db directory
cd /home/makerpm/rpmbuild || exit 1

## Creating the file structure for the SOURCE tarball
rsync -avzP --exclude ".git" --exclude ".gitlab-ci.yml" --exclude ".gitignore" --exclude ".codeclimate.yml" --exclude "build" /home/makerpm/rpmbuild/malscan-db/ "$TEMP/malscan-db-$PACKAGE_VERSION/"

## Packaging the files
cd "$TEMP" || exit 1
tar -czvf "$TEMP/malscan-db-$PACKAGE_VERSION.tar.gz" "malscan-db-$PACKAGE_VERSION"

# Moving the newly packaged files into the build sources directory
mv "$TEMP/malscan-db-$PACKAGE_VERSION.tar.gz" "/home/makerpm/rpmbuild/SOURCES/"

## Copying the latest SPEC files from our git repo into SPECS
cp "/home/makerpm/rpmbuild/malscan-db/build/malscan-db.spec" "/home/makerpm/rpmbuild/SPECS/malscan-db.spec"

## Moving back into our pwd
cd /home/makerpm/rpmbuild || exit 1

## Deleting the temp directory and all of its staging contents
rm -rf "$TEMP"

## Finishing up the source build
echo "Staging of all malscan-db files completed. Beginning build process."

## Creating the RPM
rpmbuild -ba /home/makerpm/rpmbuild/SPECS/malscan-db.spec

echo "Builds complete for Malscan-DB $PACKAGE_VERSION"

echo "Beginning RPM signing."
## Doing the RPM signing
for RPM in `ls /home/makerpm/rpmbuild/RPMS/noarch/`; do
  echo "Now signing $RPM"
  /home/makerpm/rpmbuild/malscan-db/build/sign.exp "/home/makerpm/rpmbuild/RPMS/noarch/$RPM"
done

echo "Uploading RPMs to AWS S3"
aws s3 sync "/home/makerpm/rpmbuild/RPMS/noarch/" s3://yum.malscan.org/el/7/
aws s3 sync "/home/makerpm/rpmbuild/RPMS/noarch/" s3://yum.malscan.org/el/6/
aws s3 sync "/home/makerpm/rpmbuild/RPMS/noarch/" s3://yum.malscan.org/fedora/26/
aws s3 sync "/home/makerpm/rpmbuild/RPMS/noarch/" s3://yum.malscan.org/fedora/27/
aws s3 sync "/home/makerpm/rpmbuild/RPMS/noarch/" s3://yum.malscan.org/fedora/28/
